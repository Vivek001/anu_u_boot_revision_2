#define DMA0_CRYPTO_TXFIFO 0
#define DMA0_CRYPTO_RXFIFO 1
#define DMA0_HS_TXFIFO 2
#define DMA0_HS_RXFIFO 3

#define DMA_INT
void crypto_rxfifo_isr(void)
{

 dma_retain();
 dma_enable_channel(0, (void*)0x28000000, (void*)0x00000000, DMA_CRYPTO_RXFIFO,
                DMAC_FLOWCTRL_DMAC_PERI_TO_MEM, true, true, 0xE,
                DMA_S8, NULL);


}
#endif

void crypto_init()
{
	RXCFG.SOFTRESET  = 0x0; //writing '1' generates a soft reset t0 tx fifo, until a '0' is written into this bit.
        RXCFG.INTEN      = 0x3; // Interrupt enable for fifo empty fifo full
        RXCFG.INTCLR     = 0x7; // Clear the empty interrupt, full interrupt
        RXCFG.DATAFWMARK = 0x100;
        RXCFG.LENAFWMARK = 0xE;
#define DMA_INT
	Int_Install(Crypto_RXFIFO_Interrupt,0,false,crypto_rxfifo_isr);
        Int_Enable(Crypto_RXFIFO_Interrupt);
#endif

}


void crypto_rxfifo()
{




}


void crypto_txfifo()
{


}
