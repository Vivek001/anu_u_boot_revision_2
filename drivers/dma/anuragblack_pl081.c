#define MAX_TRANSFER (4*((1<<11)-1)) /* maximum data we can transfer via DMA
                                      * i.e. 32 bits at once 
                                      * and the number of 32bits words has to
                                      * fit in 11 bits of DMA register */


static const void *dma_start_addr;    /* Pointer to callback buffer */
static size_t dma_start_size;   /* Size of callback buffer */
static const void *dma_sub_addr;      /* Pointer to sub buffer */
static size_t dma_rem_size;     /* Remaining size - in 4*32 bits */
static size_t play_sub_size;    /* size of current subtransfer */
static void dma_callback(void);
static int locked = 0;
static int channel_g=0;
static int peri_g=0,flow_controller_g=0;
static bool src_inc_g=0,dst_inc_g=0;
static bool volatile is_playing = false;
static bool play_callback_pending = false;


void anuragblack_dma_start(int channel, void *addr, void *dst, int peri,
                        int flow_controller, bool src_inc, bool dst_inc,
                        size_t size, int nwords)
{
    is_playing = true;

    dma_start_addr = addr;
    dma_start_size = size;
    dma_sub_addr = dma_start_addr;
    dma_rem_size = size;


     dma_retain();
     anuragblack_start_play();
}


void anuragblack_dma_stop(void)
{
    is_playing = false;

    dma_disable_channel(0);

    /* Ensure byte counts read back 0 */
    DMAC_CH_SRC_ADDR(0) = 0;
    dma_start_addr = NULL;
    dma_start_size = 0;
    dma_rem_size = 0;

    dma_release();

    play_callback_pending = false;
}


static void anuragblack_start_play()
{
    const void *addr = dma_sub_addr;
    size_t size = dma_rem_size;
    if(size > MAX_TRANSFER)
        size = MAX_TRANSFER;

    play_sub_size = size;

    dma_enable_channel(0, addr, dst_g, peri_g,
                flow_controller_g, src_inc_g, dst_inc_g, size >> 2,
                nwords_g, dma_callback);
}




static void dma_callback(void)
{
    dma_sub_addr += play_sub_size;
    dma_rem_size -= play_sub_size;
    play_sub_size = 0; /* Might get called again if locked */

/*    if(locked)
    {
        play_callback_pending = is_playing;
        return;
    }
*/
    if(!dma_rem_size)
    {
 	anuragblack_dma_stop()    
        return;

    }
    else
    {
        anuragblack_start_play();
    }
}

