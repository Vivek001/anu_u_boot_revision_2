/*
 * Watchdog driver for the ANURAGBLACK
 *
 */

#include <common.h>
#include <watchdog.h>
#include <asm/io.h>
#include <asm/arch/anuragblack_wdt.h>

#define CONFIG_ANURAG_HW_TIMEOUT  2
/*
 * Set the watchdog time interval.
 * Counter is 32 bit.
 */
int anuragblack_wdt_settimeout(unsigned int timeout)
{
	unsigned int reg;

	struct anuragblack_wdt *wd = (struct anuragblack_wdt *)ANURAGBLACK_WDT_BASE;

	printf("Activating WDT..\n");
		
	reg = ANURAGBLACK_WDLOAD(timeout * ANURAGBLACK_TIMEOUT_FACTOR);

	writel(reg, &wd->wdogload);

	return 0;
}

void anuragblack_wdt_reset(void)
{
	struct anuragblack_wdt *wd = (struct anuragblack_wdt *)ANURAGBLACK_WDT_BASE;

	/* clear control register */
	writel(0, &wd->wdogcontrol);

	/* Enable WDT */
	writel((ANURAGBLACK_WDCR_RST | ANURAGBLACK_WDCR_INTR), &wd->wdogcontrol);
}

void anuragblack_wdt_disable(void)
{
	struct anuragblack_wdt *wd = (struct anuragblack_wdt *)ANURAGBLACK_WDT_BASE;

	printf("Deactivating WDT..\n");

	/*
	 * It was defined with CONFIG_WATCHDOG_NOWAYOUT in Linux
	 *
	 * Shut off the timer.
	 * Lock it in if it's a module and we defined ...NOWAYOUT
	 */
	writel(0, &wd->wdogcontrol);
}

#if defined(CONFIG_HW_WATCHDOG)
void hw_watchdog_reset(void)
{
	anuragblack_wdt_reset();
}

void hw_watchdog_init(void)
{
	/* set timer in ms */
	anuragblack_wdt_settimeout(CONFIG_ANURAG_HW_TIMEOUT * 1000);
}
#endif
