/*
 * Copyright (C) 2012 Shravan Kumar
 * <shravan.alugala@moschip.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <dm.h>
#include <errno.h>
#include <asm/gpio.h>
#include <asm/io.h>

#define PLAT_PL061_MAX_GPIOS	32


#define MAX_GPIO_DEVICES	((PLAT_PL061_MAX_GPIOS +		\
				 (GPIOS_PER_PL061 - 1)) / GPIOS_PER_PL061)

#define PL061_GPIO_DIR		0x400

#define GPIOS_PER_PL061		8
#define BIT(nr)			(1UL << (nr))


static uintptr_t pl061_reg_base[MAX_GPIO_DEVICES] = {0x58006000,0x58007000,0x60001000,0x60002000};


struct anuragpl061_gpios {
	struct gpio_gen_reg *reg;
};


static int anuragblack_gpio_direction_input(struct udevice *dev, unsigned gpio)
{
	struct anuragpl061_gpios *gpios = dev_get_priv(dev);
	debug("gpio base address = %p\n",gpios);

	uintptr_t base_addr;
	unsigned int data, offset;

	assert((gpio >= 0) && (gpio < PLAT_PL061_MAX_GPIOS));

	base_addr = pl061_reg_base[gpio / GPIOS_PER_PL061];
	offset = gpio % GPIOS_PER_PL061;
		data = readb(base_addr + PL061_GPIO_DIR) & ~BIT(offset);
		writeb(data, base_addr + PL061_GPIO_DIR);

	return 0;
}

static int anuragblack_gpio_direction_output(struct udevice *dev, unsigned gpio,
					 int value)
{
	struct anuragpl061_gpios *gpios = dev_get_priv(dev);

	debug("gpio base address = %p\n",gpios);

	uintptr_t base_addr;
	unsigned int data, offset;

	assert((gpio >= 0) && (gpio < PLAT_PL061_MAX_GPIOS));

	base_addr = pl061_reg_base[gpio / GPIOS_PER_PL061];
	offset = gpio % GPIOS_PER_PL061;
		data = readb(base_addr + PL061_GPIO_DIR) | BIT(offset);
		writeb(data, base_addr + PL061_GPIO_DIR);

	return 0;
}


/*
* The offset of GPIODATA register is 0.
* The values read from GPIODATA are determined for each bit, by the mask bit
* derived from the address used to access the data register, PADDR[9:2].
* Bits that are 1 in the address mask cause the corresponding bits in GPIODATA
* to be read, and bits that are 0 in the address mask cause the corresponding
* bits in GPIODATA to be read as 0, regardless of their value.
*/
static int anuragblack_gpio_get_value(struct udevice *dev, unsigned gpio)
{
	const struct anuragpl061_gpios *gpios = dev_get_priv(dev);

	debug("gpio base address = %p\n",gpios);

	uintptr_t base_addr;
	unsigned int offset;

	assert((gpio >= 0) && (gpio < PLAT_PL061_MAX_GPIOS));

	base_addr = pl061_reg_base[gpio / GPIOS_PER_PL061];
	offset = gpio % GPIOS_PER_PL061;
	if (readb(base_addr + BIT(offset + 2)))
		return GPIO_LEVEL_HIGH;
	return GPIO_LEVEL_LOW;

}


/*
* In order to write GPIODATA, the corresponding bits in the mask, resulting
* from the address bus, PADDR[9:2], must be HIGH. Otherwise the bit values
* remain unchanged by the write.
*/
static int anuragblack_gpio_set_value(struct udevice *dev, unsigned gpio,
				  int value)
{
	struct anuragpl061_gpios *gpios = dev_get_priv(dev);

	debug("gpio base address = %p\n",gpios);
	uintptr_t base_addr;
	int offset;

	assert((gpio >= 0) && (gpio < PLAT_PL061_MAX_GPIOS));

	base_addr = pl061_reg_base[gpio / GPIOS_PER_PL061];
	offset = gpio % GPIOS_PER_PL061;
	if (value == GPIO_LEVEL_HIGH)
		writeb(BIT(offset), base_addr + BIT(offset + 2));
	else
		writeb(0,base_addr + BIT(offset + 2));

	return 0;
}

static int anuragblack_gpio_get_function(struct udevice *dev, unsigned gpio)
{

	struct anuragpl061_gpios *gpios = dev_get_priv(dev);
	debug("gpio base address = %p\n",gpios);

	uintptr_t base_addr;
	unsigned int data, offset;

	assert((gpio >= 0) && (gpio < PLAT_PL061_MAX_GPIOS));

	base_addr = pl061_reg_base[gpio / GPIOS_PER_PL061];
	offset = gpio % GPIOS_PER_PL061;
	data = readb(base_addr + PL061_GPIO_DIR);
	if (data & BIT(offset))
		return GPIOF_OUTPUT;
	return GPIOF_INPUT;

}


static const struct dm_gpio_ops anuragblack_ops = {
	.direction_input	= anuragblack_gpio_direction_input,
	.direction_output	= anuragblack_gpio_direction_output,
	.get_value		= anuragblack_gpio_get_value,
	.set_value		= anuragblack_gpio_set_value,
	.get_function		= anuragblack_gpio_get_function,
};

static int anuragblack_gpio_probe(struct udevice *dev)
{
	struct anuragpl061_gpios *gpios = dev_get_priv(dev);
	struct anurag_gpio_platdata *plat = dev_get_platdata(dev);
	struct gpio_dev_priv *uc_priv = dev_get_uclass_priv(dev);

	uc_priv->bank_name = "GPIO";
	uc_priv->gpio_count = ANURAG_GPIO_COUNT;
	gpios->reg = (struct gpio_gen_reg *)plat->base;

	debug("gpios = %p base = %p \n",(void *)gpios,(void *)plat->base);

	return 0;
}

U_BOOT_DRIVER(gpio_bcm2835) = {
	.name	= "gpio_anuragblack",
	.id	= UCLASS_GPIO,
	.ops	= &anuragblack_ops,
	.probe	= anuragblack_gpio_probe,
	.priv_auto_alloc_size = sizeof(struct anuragpl061_gpios),
};
