/*
 * Copyright (C) 2012 Samsung Electronics
 * R. Chandrasekar <rcsekar@samsung.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <sound.h>
#include <malloc.h>
#include <asm/arch/anuragblack_i2s.h>

/* defines */
#define SOUND_400_HZ 400
#define SOUND_BITS_IN_BYTE 8
#define LEFT            1
#define RIGHT           2
#define LEFT_RIGHT      3
#define DATA_OFFSET     8


void sound_create_square_wave(unsigned short *data, int size, uint32_t freq)
{
	const int sample = 48000;
	const unsigned short amplitude = 16000; /* between 1 and 32767 */
	const int period = freq ? sample / freq : 0;
	const int half = period / 2;

	assert(freq);

	/* Make sure we don't overflow our buffer */
	if (size % 2)
		size--;

	while (size) {
		int i;
		for (i = 0; size && i < half; i++) {
			size -= 2;
			*data++ = amplitude;
			*data++ = amplitude;
		}
		for (i = 0; size && i < period - half; i++) {
			size -= 2;
			*data++ = -amplitude;
			*data++ = -amplitude;
		}
	}
}


int sound_init(const void *blob )
{
	int ret ;

#ifdef CODEC_INIT
	codec_init(); // Fixme 
#endif

        ret = anuragblack_i2s_tx_init();
        if (ret) {
                printf("ERROR: Failed to init I2S");
                return ret;
        }
        return ret;
}


int sound_play(uint32_t msec, uint32_t frequency)
{


	unsigned int *data;
        unsigned long data_size;
        unsigned int ret = 0;

        /*Buffer length computation */
        data_size = sound_params.freq * sound_params.channels;
        data_size *= (sound_params.bps / SOUND_BITS_IN_BYTE);
        data = malloc(data_size);

        if (data == NULL) {
                debug("%s: malloc failed\n", __func__);
                return -1;
        }


        sound_create_square_wave((unsigned short *)data,
                        data_size / sizeof(unsigned short), sound_params.freq);



        while (msec >= 1000) {
                ret = anuragblack_i2s_transfer_tx_data(data, (data_size / sizeof(int)),MONO);
                msec -= 1000;
        }
        if (msec) {
                unsigned long size =
                        (data_size * msec) / (sizeof(int) * 1000);

                ret = anuragblack_i2s_transfer_tx_data(data, size,MONO);
        }
        free(data);


return ret;

}
