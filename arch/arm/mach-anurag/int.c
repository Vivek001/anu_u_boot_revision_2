#include <common.h>
#include <div64.h>
#include <linux/types.h>        /* for size_t */
#include <linux/stddef.h>       /* for NULL */
#include <asm/io.h>
#include <asm/arch/int_exported.h>
#include <asm/arch/int.h>
#include <asm/arch/vic0_reg.h>
#include <asm/arch/vic1_reg.h>


#define IRQ_REG_SAVE __interrupt

static int current_irq;

/**
 * The vectored interrupt flow is described in section 2.2.1 in the ARM PrimeCell Vectored Interrupt
 * Controller (PL192) Technical Reference Manual.  Note that this handler is only called if VIC mode is
 * turned off.
 */
 void Int_IrqHandler(void)
{
    INT_HANDLER handler;

  //  FIQ_DISABLE(); Fixme

    // In the case of IRQ exception, the LR is pointing initially to the last executed instruction + 8

    uint32_t irqStatus0 = *REG_VIC0IRQSTATUS;
    uint32_t irqStatus1 = *REG_VIC1IRQSTATUS;

   for (current_irq = 0; irqStatus0; current_irq++, irqStatus0 >>= 1)
      if(irqStatus0 & 1) {
   
    // The VICADDRESS register contains the address of the ISR to execute.
    handler = (INT_HANDLER)(*REG_VIC0ADDRESS);
    assert(handler); /// ASSERT_HS003 - Invalid IRQ handler
    // Execute the ISR. Never returns.
    handler();
    }

 for (current_irq = 32; irqStatus1; current_irq++, irqStatus1 >>= 1)
      if(irqStatus1 & 1) {
    
    // The VICADDRESS register contains the address of the ISR to execute.
    handler = (INT_HANDLER)(*REG_VIC1ADDRESS);
    assert(handler); /// ASSERT_HS003 - Invalid IRQ handler
    // Execute the ISR. Never returns.
    handler();
    }


   
    // Writing to the VICADDRESS register clears the interrupt at the VIC level.
    // Any value can be written to VICADDRESS for clearing the interrupt.
    *REG_VIC0ADDRESS = 0;
    *REG_VIC1ADDRESS = 0;

}


int arch_interrupt_init(void)
{
  Int_Initialize();
  return 0;
}



void Int_Initialize(void)
{
    uint32_t index;

//   VIC 0
    *REG_VIC0INTSELECT       = 0;
    *REG_VIC0INTENCLEAR      = 0xFFFFFFFF; // Clear enable bits in VICINTENABLE
    *REG_VIC0SOFTINTCLEAR    = 0xFFFFFFFF; // Clear enable bits in VICSOFTINT
    *REG_VIC0PROTECTION      = 0;
    *REG_VIC0SWPRIORITYMASK  = 0xFFFF;     // Enable all 16 priority levels
    *REG_VIC0PRIORITYDAISY   = 0;
    *REG_VIC0ADDRESS         = 0;

    // Set registers to default values
    for(index = 0; index < TOTAL_INT_IDS; index++ )
    {
        *REG_VIC0VECTADDR(index)     = 0;
        *REG_VIC0VECTPRIORITY(index) = 0;
    }

//   VIC 1

    *REG_VIC1INTSELECT       = 0;
    *REG_VIC1INTENCLEAR      = 0xFFFFFFFF; // Clear enable bits in VICINTENABLE
    *REG_VIC1SOFTINTCLEAR    = 0xFFFFFFFF; // Clear enable bits in VICSOFTINT
    *REG_VIC1PROTECTION      = 0;
    *REG_VIC1SWPRIORITYMASK  = 0xFFFF;     // Enable all 16 priority levels
    *REG_VIC1PRIORITYDAISY   = 0;
    *REG_VIC1ADDRESS         = 0;

    // Set registers to default values
    for(index = 0; index < TOTAL_INT_IDS; index++ )
    {
        *REG_VIC1VECTADDR(index)     = 0;
        *REG_VIC1VECTPRIORITY(index) = 0;
    }

}



INT_HANDLER Int_Install(INT_ID_e  interruptId,uint32_t priority,bool  isFiq,INT_HANDLER handler)
{
	INT_HANDLER old_handler;

	assert(interruptId < TOTAL_INT_IDS); //
	assert(handler); //


	if(interruptId < 32) // VIC0
	{
		old_handler = (INT_HANDLER)*REG_VIC0VECTADDR(interruptId);
		*REG_VIC0VECTADDR(interruptId) = (uint32_t) handler;
		// Note that this parameter is not used for FIQ
		*REG_VIC0VECTPRIORITY(interruptId) = priority;


		if(isFiq)
		{
			*REG_VIC0INTSELECT |= (1 << interruptId);
		}
		else
		{
			*REG_VIC0INTSELECT &= ~(1 << interruptId);
		}
	}
	else 
	{ // VIC1

		interruptId = interruptId - 32 ;

		old_handler = (INT_HANDLER)*REG_VIC1VECTADDR(interruptId);
		*REG_VIC1VECTADDR(interruptId) = (uint32_t) handler;
		// Note that this parameter is not used for FIQ
		*REG_VIC1VECTPRIORITY(interruptId) = priority;


		if(isFiq)
		{
			*REG_VIC1INTSELECT |= (1 << interruptId);
		}
		else
		{
			*REG_VIC1INTSELECT &= ~(1 << interruptId);
		}


	}

	return ( old_handler );
}



/**
 * Enables the specified interrupt.
 *
 * @param   interruptId   Source interrupt number.
 */

void Int_Enable(INT_ID_e interruptId)
{
	if(interruptId < 32) //VIC0
	{
		*REG_VIC0INTENABLE = (1UL << (interruptId));
	}
	else
	{ // VIC1
		interruptId = interruptId - 32 ;
		*REG_VIC1INTENABLE = (1UL << (interruptId));
	}
}


/**
 * Disable the specified interrupt.
 *
 * @param   interruptId   Source interrupt number.
 */
void Int_Disable(INT_ID_e interruptId)
{

	if(interruptId < 32) //VIC0
	{
		*REG_VIC0INTENCLEAR = (1UL << (interruptId));
	}
	else //VIC1
	{
		interruptId = interruptId - 32 ;
		*REG_VIC1INTENABLE = (1UL << (interruptId));
	}
}




