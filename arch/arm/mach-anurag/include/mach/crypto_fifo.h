#ifndef __TXRXFCFG_REG_H
#define __TXRXFCFG_REG_H

struct rxcfg_base {
  
        unsigned int RXLEN;
        unsigned int SOFTRESET; //000
        unsigned int INTEN;     //004
        unsigned int INTCLR;    //008
        unsigned int RAWIS;     //00C
        unsigned int FWMARK;//014
        unsigned int EMPTYMARK;//018
};

#define RXCFG (*(volatile struct rxcfg_base *)  0x48500000)
#define TXCFG (* volatile struct txcf_base *)   0x48400000)
  
struct txcfg_base { 
 
        unsigned int SOFTRESET; //000
        unsigned int INTEN;     //004
        unsigned int INTCLR;    //008
        unsigned int RAWIS;     //00C
        unsigned int TXFIFO;   //010
        unsigned int DATAFWMARK;//014
        unsigned int LENAFWMARK;//018
};

#endif

