/*
 * Copyright (C) 2012 Vikram Narayananan
 * <vikram186@gmail.com>
 * (C) Copyright 2012,2015 Stephen Warren
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */



#ifndef __GPIO_REG_H
#define __GPIO_REG_H



#ifndef CONFIG_anurag
#define ANURAG_GPIO_BASE                0x58006000
#else
#define ANURAG_GPIO_BASE                0x58006000
#endif
#define ANURAG_GPIO_COUNT               32



#define GPIO_DIR_OUT		0
#define GPIO_DIR_IN		1

#define GPIO_LEVEL_LOW		0
#define GPIO_LEVEL_HIGH		1



struct gpio_data {

        unsigned int GPIODATA0;
        unsigned int GPIODATA1;
        unsigned int GPIODATA2;
};

struct gpio_data_3fc {

        unsigned int GPIODATA3FC;
};

struct gpio_gen_reg {
      unsigned int GPIODIR;
      unsigned int GPIOIS;
      unsigned int GPIOIBE;
      unsigned int GPIOIEV;
      unsigned int GPIOIE;
      unsigned int GPIORIS;
      unsigned int GPIOMIS;
      unsigned int GPIOIC;
      unsigned int GPIOAFSEL;
};


struct gpio_per_id {

        unsigned int GPIOPERPHID0;
        unsigned int GPIOPERPHID1;
        unsigned int GPIOPERPHID2;
        unsigned int GPIOPERPHID3;
        unsigned int GPIOPCELLID0;
        unsigned int GPIOPCELLID1;
        unsigned int GPIOPCELLID2;
        unsigned int GPIOPCELLID3;
};

struct anurag_gpio_platdata {
	unsigned int base;
};

#endif
