#ifndef INTERRUPTS_EXPORTED_H
#define INTERRUPTS_EXPORTED_H

//#define IRQ_ENABLE()                 __asm("CPSIE i")
//#define IRQ_DISABLE()                __asm("CPSID i")
//#define FIQ_ENABLE()                 __asm("CPSIE f")
//#define FIQ_DISABLE()                __asm("CPSID f")


typedef void (*INT_HANDLER)(void);


typedef enum INT_ID
{
Watchdog_Interrupt,
Software_Interrupt,
Comms_Rx_Interrupt,
Comms_Tx_Interrupt,
RTC_Interrupt,
External_Interrupt_0,
External_Interrupt_1, //6
External_Interrupt_2, //7
External_Interrupt_3, //8
External_Interrupt_4, //9
External_Interrupt_5, //10
Alternate_Processor_Interrupt_0, //11
Alternate_Processor_Interrupt_1, //12
Alternate_Processor_Interrupt_2, //13
Alternate_Processor_Interrupt_3, //14
Alternate_Processor_Interrupt_4, //15
Alternate_Processor_Interrupt_5, //16
ARM1176_DMA_Interrupt, //17
ARM1176_Performance_Management_Unit_Interrupt, //18
ARM1176_DMA_Error_Interrupt, //19
System_Logic_Interrupt, //20
PL301_interrupt, //21
UART6_crypto_Interrupt, //22
UART7_crypto_Interrupt, //23
DMAC0_interrupt, //24
DMAC1_interrupt, //25
DMAC2_interrupt, //26
Crypto_TXFIFO_Interrupt, //27
Crypto_RXFIFO_Interrupt, //28
HS_TXFIFO_Interrupt, //29
HS_RXFIFO_Interrupt, //30
Ethernet0_Interrupt, //31 -> alias 31 on VIC0
Ethernet1_Interrupt, //32 -> alias 0 in VIC1
EMPTY1, //33
EMPTY2, //34
EMPTY3, //35
L2CC_Interrupt, //36
SPI0_Interrupt, //37
SPI1_Interrupt, //38
I2S0_Interrupt, //39
I2S1_Interrupt, //40
UART0_Interrupt, //41
UART1_Interrupt, //42
UART2_Interrupt, //43
UART3_Interrupt, //44
I2C0_Interrupt, //45
I2C1_Interrupt, //46
GPIO0_Interrupt, //47
GPIO1_Interrupt, //48
Timer0_Unique_interrupt, //49
Timer1_Unique_interrupt, //50
Timer2_interrupt, //51
EMPTY4, //52
GPIO2_Interrupt, //53
GPIO3_Interrupt, //54
UART4_Interrupt, //55
UART5_Interrupt, //56
SPI2_Interrupt, //57
SPI3_Interrupt, //58
I2C2_Interrupt, //59
I2C3_Interrupt, //60
DDR2_Ctrl_Ext_SRAM_Ctrl_Interrupt, //61
NOR_Flash_Ctrl_Interrupt, //62
On_chip_SRAM_Ctrl_Interrupt, //63
TOTAL_INT_IDS
}INT_ID_e;


#endif
