#ifndef _INTERRUPTS_H_
#define _INTERRUPTS_H_

void Int_Initialize(void);
INT_HANDLER Int_Install(INT_ID_e interrupt, uint32_t priority, bool isFiq, INT_HANDLER handler);
void Int_IrqHandler(void);
void Int_FiqHandler(void);
void Fault_Handler(void);
void Int_Enable(INT_ID_e interruptId);
void Int_Disable(INT_ID_e interruptId);
uint32_t Int_Enabled(INT_ID_e interruptId);
bool Int_IsPending(INT_ID_e interruptId);
bool Int_IsActive(INT_ID_e interruptId, bool isFiq);
bool Int_IsEnabled(INT_ID_e interruptId);
uint8_t Int_GetPriority(INT_ID_e interruptId);


#endif
