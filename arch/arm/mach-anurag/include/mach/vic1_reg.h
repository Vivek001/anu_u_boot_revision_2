/*
* Author : shravan_alugala@moschip.com 
 */
/**
 * @file
 * VIC Registers
 * @ingroup halSoC
 * @addtogroup halSoC
 * @{
 */
#ifndef VIC1_REGS_H
#define VIC1_REGS_H

/**
 *  
 *  Please refer to the enum INT_ID_e for details.
 */

#define VIC1_INT_PRIORITIES    (16)

#define VIC1_REGISTER_BASE       ((uint32_t)0x48100000)

#define VIC1_REGISTER_OFFSET(x)  ((volatile uint32_t *)(VIC1_REGISTER_BASE + ((x) * 4)))

/**
 * IRQ Status Register
 *  This register provides the status of interrupts [31:0] after IRQ masking
 *        '0' = interrupt is inactive, and '1' = interrupt is active
 */
#define REG_VIC1IRQSTATUS            VIC1_REGISTER_OFFSET(0)

/**
 * FIQ Status Register               
 *  This register provides the status of interrupts after FIQ masking
 *        '0' = interrupt is inactive, and '1' = interrupt is active
 */
#define REG_VIC1FIQSTATUS            VIC1_REGISTER_OFFSET(1)

/**
 * Raw Interrupt Status Register 
 *  This register provides the status of unmasked status of the interrupt sources
 *        '0' = interrupt is inactive, and '1' = interrupt is active
 */
#define REG_VIC1RAWINTR              VIC1_REGISTER_OFFSET(2)

/**
 * Interrupt Select Register
 *  This register selects whether the corresponding interrupt source generates
 *        an FIQ or IRQ interrupt
 *        '0' = IRQ, and '1' = FIQ 
 */
#define REG_VIC1INTSELECT            VIC1_REGISTER_OFFSET(3)

/**
 * Interrupt Enable Register
 *  This register enables the interrupt lines
 *        write: '0' = no effect, and '1' = interrupt enabled 
 *        read: '0' = interrupt disabled, and '1' = interrupt enabled 
 */
#define REG_VIC1INTENABLE            VIC1_REGISTER_OFFSET(4)

/**
 * Interrupt Enable Clear Register
 *  This register clears bits in the REG_VICINTENABLE register, and
 *        masks out the interrupt sources for the IRQ interrupt.
 *        '0' = no effect, and '1' = interrupt disabled in the REG_VICINTENABLE register
 */
#define REG_VIC1INTENCLEAR           VIC1_REGISTER_OFFSET(5)

/**
 * Software Interrupt Register
 *  This register is used to generate software interrupts
 *        write: '0' = no effect, and '1' = software interrupt active 
 *        read: '0' = software interrupt inactive, and '1' = software interrupt active 
 */
#define REG_VIC1SOFTINT              VIC1_REGISTER_OFFSET(6)

/**
 * Software Interrupt Clear Register
 *  This register clears bits in REG_VICSOFTINT register
 *        '0' = no effect, and '1' = software interrupt disabled in the REG_VICSOFTINT register
 */
#define REG_VIC1SOFTINTCLEAR         VIC1_REGISTER_OFFSET(7)

/**
 * Protection Enable Register
 *  This register enables or disables protected register access, stopping register access
 *        when the processor is in User mode  
 *        '0' = protection mode disabled, and '1' = protection mode enabled 
 */
#define REG_VIC1PROTECTION           VIC1_REGISTER_OFFSET(8)

/**
 * Software Priority Mask Register
 *  This register contains the mask value for the interrupt priority levels
 *        '0' = priority level is masked, and '1' = priority level is not masked 
 */
#define REG_VIC1SWPRIORITYMASK       VIC1_REGISTER_OFFSET(9)
   #define VIC1SWPRIORITYMASK_SWPRIORITYMASK (0xFFFF)
   #define VIC1SWPRIORITYMASK(swprioritymask) (vectpriority & VICSWPRIORITYMASK_SWPRIORITYMASK)

/**
 * Vector Priority Register for Daisy
 *  This register selects the interrupt priority level for the daisy chain input.
 *        The value can be from 0-15.  Default value:  15 (lowest)
 */
#define REG_VIC1PRIORITYDAISY        VIC1_REGISTER_OFFSET(0xA)
   #define VIC1PRIORITYDAISY_VECTPRIORITY_MASK (0xF)
   #define VIC1PRIORITYDAISY(vectpriority) (vectpriority & VICPRIORITYDAISY_VECTPRIORITY_MASK)
   
/**
 * Vector Address 0 - 31 Registers
 *  This registers contain the ISR vector addresses
 */
#define REG_VIC1VECTADDR(X) VIC1_REGISTER_OFFSET(0x40 + X)

/**
 * Vector Priority 0 - 31 Registers
 *  This registers select the interrupt priority level for the 32 vectored interrupt 
 *        sources.  The value can be from 0-15.  Default value:  15 (lowest)
 */
#define REG_VIC1VECTPRIORITY(X) VIC1_REGISTER_OFFSET(0x80 + X)
   #define VIC1VECTPRIORITY_VECTPRIORITY_MASK (0xF)
   #define VIC1VECTPRIORITY(vectpriority) (vectpriority & VIC1VECTPRIORITY_VECTPRIORITY_MASK)

/**
 * Vector Address Register
 *  This register contains the ISR address of the current active interrupt.  If no
 *        interrupt is currently active, the register holds the ISR address of the last
 *        active interrupt
 *        reset value:0
 */
#define REG_VIC1ADDRESS VIC1_REGISTER_OFFSET(0x3C0)

/**
 * Peripheral Identification Register
 *  This register provides the following information:
 *        [11:0]  part number
 *        [19:12] designer 
 *        [23:20] revision number 
 *        [31:24] configuration 
 */
#define REG_VIC1PERIPHID0 VIC1_REGISTER_OFFSET(0x3F8)
   #define VIC1PERIPHID0_PARTNUMBER0_MASK (0xFF)
   #define VIC1PERIPHID0_PARTNUMBER0 (REG_VICPERIPHID0 & VIC1PERIPHID0_PARTNUMBER0_MASK)

#define REG_VIC1PERIPHID1 VIC1_REGISTER_OFFSET(0x3F9)
   #define VIC1PERIPHID1_DESIGNER0_MASK (0xF)
   #define VIC1PERIPHID1_DESIGNER0_SHIFT (4)
   #define VIC1PERIPHID1_DESIGNER0 (REG_VICPERIPHID1 & VIC1PERIPHID1_DESIGNER0_MASK)
   #define VIC1PERIPHID1_PARTNUMBER1_MASK (0xF)
   #define VIC1PERIPHID1_PARTNUMBER1 (REG_VICPERIPHID1 & VIC1PERIPHID1_PARTNUMBER1_MASK)

#define REG_VIC1PERIPHID2 VIC1_REGISTER_OFFSET(0x3FA)
   #define VIC1PERIPHID2_REVISION_MASK (0xF)
   #define VIC1PERIPHID2_REVISION_SHIFT (4)
   #define VIC1PERIPHID2_DESIGNER1_MASK (0xF)

#define REG_VIC1PERIPHID3 VIC1_REGISTER_OFFSET(0x3FB)
   #define VIC1PERIPHID3_CONFIGURATION_MASK (0xF)

/**
 * PrimeCell Identification Register
 *  Hard-coded registers which contain the part number information
 *        REG_VICPCELLID0 [7:0] part number 0:0x192
 *        REG_VICPCELLID1 [7:4] designer 0:0x01
 *                        [3:0] part number 1:0x01
 *        REG_VICPCELLID2 [7:4] revision:0-15
 *                        [3:0] designer 1:0x04
 *        REG_VICPCELLID3 [7:2] configuration:0
 *                        [1:0] configuration:0 (32 interrupts supported)
 */
#define REG_VIC1PCELLID0 VIC1_REGISTER_OFFSET(0x3FC)
#define REG_VIC1PCELLID1 VIC1_REGISTER_OFFSET(0x3FD)
#define REG_VIC1PCELLID2 VIC1_REGISTER_OFFSET(0x3FE)
#define REG_VIC1PCELLID3 VIC1_REGISTER_OFFSET(0x3FF)

#endif // VIC_REGS_H

/** @} */

