/*
 * (C) Copyright 2012-2016 Stephen Warren
 *
 * SPDX-License-Identifier:	GPL-2.0
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#include <linux/sizes.h>
#include <asm/arch/timer.h>

#if defined(CONFIG_TARGET_RPI_2) || defined(CONFIG_TARGET_RPI_3_32B)
#define CONFIG_SKIP_LOWLEVEL_INIT
#endif

#ifdef CONFIG_BCM2835
#define CONFIG_SYS_CACHELINE_SIZE		32
#else
#define CONFIG_SYS_CACHELINE_SIZE		64
#endif

/* Architecture, CPU, etc.*/
#define CONFIG_ARCH_CPU_INIT

/* Use SoC timer for AArch32, but architected timer for AArch64 */
#ifndef CONFIG_ARM64
#define CONFIG_SYS_TIMER_RATE		1000000 
#define CONFIG_SYS_TIMER_COUNTER	\
	(&((struct bcm2835_timer_regs *)BCM2835_TIMER_PHYSADDR)->clo)

#endif

/*
 * 2835 is a SKU in a series for which the 2708 is the first or primary SoC,
 * so 2708 has historically been used rather than a dedicated 2835 ID.
 *
 * We don't define a machine type for bcm2709/bcm2836 since the RPi Foundation
 * chose to use someone else's previously registered machine ID (3139, MX51_GGC)
 * rather than obtaining a valid ID:-/
 *
 * For the bcm2837, hopefully a machine type is not needed, since everything
 * is DT.
 */
#ifdef CONFIG_anurag
#define CONFIG_MACH_TYPE		MACH_TYPE_ANURAG
#endif

/* Memory layout */
#define CONFIG_NR_DRAM_BANKS		1
#define CONFIG_SYS_SDRAM_BASE		0xFFF40000
#ifdef CONFIG_ARM64
#define CONFIG_SYS_TEXT_BASE		0x00080000 
#else
#define CONFIG_SYS_TEXT_BASE		0xFFF40000
#endif
#define CONFIG_SYS_UBOOT_BASE		CONFIG_SYS_TEXT_BASE
/*
 * The board really has 256M. However, the VC (VideoCore co-processor) shares
 * the RAM, and uses a configurable portion at the top. We tell U-Boot that a
 * smaller amount of RAM is present in order to avoid stomping on the area
 * the VC uses.
 */
#define CONFIG_SYS_SDRAM_SIZE		SZ_8M /* 16M */
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_SDRAM_BASE + \
					 CONFIG_SYS_SDRAM_SIZE - \
					 GENERATED_GBL_DATA_SIZE )
#define CONFIG_SYS_MALLOC_LEN		SZ_1M  /* 1M */
/*#define CONFIG_SYS_MEMTEST_START	0x00100000
#define CONFIG_SYS_MEMTEST_END		0x00200000
#define CONFIG_LOADADDR			0x00200000
*/



/*
// Flash 
#undef CONFIG_SYS_NO_FLASH
# define CONFIG_SYS_FLASH_BASE          0x50002000
# define CONFIG_SYS_FLASH_SIZE          (16 * 1024 * 1024)
#define CONFIG_FLASH_CFI_DRIVER 
#define CONFIG_SYS_CFI_FLASH_STATUS_POLL // fix amd flash issue
#define CONFIG_SYS_FLASH_CFI
#define CONFIG_SYS_FLASH_USE_BUFFER_WRITE
#define CONFIG_SYS_FLASH_PROTECTION
#define CONFIG_SYS_MAX_FLASH_BANKS_DETECT       1 
#define CONFIG_SYS_MAX_FLASH_SECT       512
#define CONFIG_SYS_FLASH_ERASE_TOUT    1000
#define CONFIG_SYS_FLASH_WRITE_TOUT    5000
#define CONFIG_SYS_MONITOR_BASE	0
#define CONFIG_CMD_FLASH
*/

/* Flash */
#define CONFIG_SYS_NO_FLASH

/* NOR */
/*
#define CONFIG_MTD
#define CONFIG_MTD_DEVICE            1 
#define CONFIG_MTD_PARTITIONS        1

#undef CONFIG_SYS_NO_FLASH // Define if u don't want flash

#ifndef CONFIG_SYS_NO_FLASH
                #define CONFIG_INIT_NOR                 1
                #undef CONFIG_BOOT_XIP 
                #define CONFIG_SYS_MAX_FLASH_BANKS      1
                // max number of sectors/blocks on one chip 
		#define CONFIG_SYS_FLASH_SIZE           (128 * 1024 * 1024)
                #define CONFIG_SYS_MAX_FLASH_SECT       516
		#define CONFIG_SYS_FLASH_ERASE_TOUT     1000
		#define CONFIG_SYS_FLASH_WRITE_TOUT     5000
		#define CONFIG_FLASH_SHOW_PROGRESS      10
                #define CONFIG_FLASH_CFI_DRIVER         1
                #define CONFIG_SYS_FLASH_CFI
                #define CONFIG_SYS_FLASH_CFI_WIDTH      FLASH_CFI_16BIT
                #undef CONFIG_SYS_FLASH_EMPTY_INFO
                #define CONFIG_SYS_FLASH_PROTECTION // don't use hardware protection 
                #define CONFIG_SYS_FLASH_USE_BUFFER_WRITE // use buffered writes (20x faster) 

                #ifdef CONFIG_MTD
                        #define CONFIG_FLASH_CFI_MTD     1
                #endif                 
               #define CONFIG_SYS_FLASH_BASE             0xF8000000
	       #define CONFIG_CMD_FLASH // for flinfo 

#endif
*/

/* DDR */
#define CONFIG_DDR_DENALI
#define CONFIG_DDR_DENALIREGS 158
#define CONFIG_DDR_BASE 0x48700000



/* Devices */
/* GPIO */
#define CONFIG_ANURAGBLACK_GPIO
/* LCD */
/*#define CONFIG_LCD
#define CONFIG_LCD_DT_SIMPLEFB
#define LCD_BPP				LCD_COLOR16 */
/*
 * Prevent allocation of RAM for FB; the real FB address is queried
 * dynamically from the VideoCore co-processor, and comes from RAM
 * not owned by the ARM CPU.
 */
/*#define CONFIG_FB_ADDR			0
#define CONFIG_VIDEO_BCM2835
#define CONFIG_SYS_WHITE_ON_BLACK
#define CONFIG_CONSOLE_SCROLL_LINES	10 */

/* I2C  */
/*#define CONFIG_SYS_I2C
#define CONFIG_SYS_I2C_BASE             0xD0180000
#define CONFIG_SYS_I2C_SPEED            400000
*/
/* SPI */
#define CONFIG_ANURAGBLACK_SPI
#define CONFIG_CMD_SPL
#define anuragblack_spi0 0x50000000
#define anuragblack_spi1 0x50001000
#define anuragblack_spi2 0x50007000
#define anuragblack_spi3 0x50008000

/* RTC */
#define CONFIG_RTC_PL031 1 /* use ARM AMBA PL031 RTC */ 
#define CONFIG_SYS_RTC_PL031_BASE 0x60003000  /* RTC Registers Base Address */ 
#define CONFIG_CMD_DATE

/* WATCHDOG */
/*#define CONFIG_ANURAGBLACK_WATCHDOG
#define CONFIG_WDT_TEST
#define ANURAGBLACK_WDT_BASE 0x60004000
#define CONFIG_SYS_CLK_FREQ 0x40000000 // external freq to pll 
*/

/* TIMER */
/*#define SHIKHARA_DT0_BASE 0x5800C000
#define SHIKHARA_DT1_BASE 0x5800D000
*/
/* DMA */
/*#define CONFIG_DMA_PL081 */


/* SOUND */
#define CONFIG_SOUND 
#define CONFIG_I2S_ANURAGBLACK
#define CONFIG_CMD_SOUND
#define ANURAGBLACK_I2S_BASE 0x50005000 
#define I2S0 0x50005000
#define I2S1 0x50006000

/*INTERRUPT SUPPORT FLAGS */
/*#define CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ            0x80000 //512KB is set for irq stack
#define CONFIG_STACKSIZE_FIQ            4096
*/
/* SD/MMC configuration */
#define CONFIG_GENERIC_MMC
#define CONFIG_MMC
#define CONFIG_SDHCI
#define CONFIG_MMC_SDHCI_IO_ACCESSORS
#define CONFIG_BCM2835_SDHCI

#ifdef CONFIG_CMD_USB
#define CONFIG_USB_DWC2
#ifndef CONFIG_anurag
#define CONFIG_USB_DWC2_REG_ADDR 0x3f980000
#else
#define CONFIG_USB_DWC2_REG_ADDR 0x20980000
#endif
#define CONFIG_USB_STORAGE
#define CONFIG_USB_HOST_ETHER
#define CONFIG_USB_ETHER_SMSC95XX
#define CONFIG_TFTP_TSIZE
#define CONFIG_MISC_INIT_R
#define CONFIG_USB_KEYBOARD
#define CONFIG_SYS_USB_EVENT_POLL
#define CONFIG_SYS_STDIO_DEREGISTER
#endif

/* Console UART */
#ifdef CONFIG_anurag
#define CONFIG_PL01X_SERIAL
#define CONFIG_CONS_INDEX		0
#define CONFIG_BAUDRATE			115200
#endif

/* Console configuration */
#define CONFIG_SYS_CBSIZE		1024
#define CONFIG_SYS_PBSIZE		(CONFIG_SYS_CBSIZE +		\
					 sizeof(CONFIG_SYS_PROMPT) + 16)

/* Environment */
#define CONFIG_ENV_SIZE			SZ_16K
#define CONFIG_ENV_IS_IN_FAT
#define FAT_ENV_INTERFACE		"mmc"
#define FAT_ENV_DEVICE_AND_PART		"0:1"
#define FAT_ENV_FILE			"uboot.env"
#define CONFIG_FAT_WRITE
#define CONFIG_ENV_VARS_UBOOT_CONFIG 
#define CONFIG_SYS_LOAD_ADDR		0x1000000
#define CONFIG_CONSOLE_MUX
#define CONFIG_SYS_CONSOLE_IS_IN_ENV
#define CONFIG_PREBOOT			"usb start"

/* Shell */
#define CONFIG_SYS_MAXARGS		16
#define CONFIG_COMMAND_HISTORY

/* Commands */
#define CONFIG_PARTITION_UUIDS
#define CONFIG_CMD_PART

/* ATAGs support for bootm/bootz */
#define CONFIG_SETUP_MEMORY_TAGS
#define CONFIG_CMDLINE_TAG
#define CONFIG_INITRD_TAG

#include <config_distro_defaults.h>

/* Environment */
#define CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
#define ENV_DEVICE_SETTINGS \
	"stdin=serial,usbkbd\0" \
	"stdout=serial,lcd\0" \
	"stderr=serial,lcd\0"

/*
 * Memory layout for where various images get loaded by boot scripts:
 *
 * I suspect address 0 is used as the SMP pen on the RPi2, so avoid this.
 *
 * fdt_addr_r simply shouldn't overlap anything else. However, the RPi's
 *   binary firmware loads a DT to address 0x100, so we choose this address to
 *   match it. This allows custom boot scripts to pass this DT on to Linux
 *   simply by not over-writing the data at this address. When using U-Boot,
 *   U-Boot (and scripts it executes) typicaly ignore the DT loaded by the FW
 *   and loads its own DT from disk (triggered by boot.scr or extlinux.conf).
 *
 * pxefile_addr_r can be pretty much anywhere that doesn't conflict with
 *   something else. Put it low in memory to avoid conflicts.
 *
 * kernel_addr_r must be within the first 128M of RAM in order for the
 *   kernel's CONFIG_AUTO_ZRELADDR option to work. Since the kernel will
 *   decompress itself to 0x8000 after the start of RAM, kernel_addr_r
 *   should not overlap that area, or the kernel will have to copy itself
 *   somewhere else before decompression. Similarly, the address of any other
 *   data passed to the kernel shouldn't overlap the start of RAM. Pushing
 *   this up to 16M allows for a sizable kernel to be decompressed below the
 *   compressed load address.
 *
 * scriptaddr can be pretty much anywhere that doesn't conflict with something
 *   else. Choosing 32M allows for the compressed kernel to be up to 16M.
 *
 * ramdisk_addr_r simply shouldn't overlap anything else. Choosing 33M allows
 *   for any boot script to be up to 1M, which is hopefully plenty.
 */
#define ENV_MEM_LAYOUT_SETTINGS \
	"fdt_high=ffffffff\0" \
	"initrd_high=ffffffff\0" \
	"fdt_addr_r=0x00000100\0" \
	"pxefile_addr_r=0x00100000\0" \
	"kernel_addr_r=0x01000000\0" \
	"scriptaddr=0x02000000\0" \
	"ramdisk_addr_r=0x02100000\0" \

#define BOOT_TARGET_DEVICES(func) \
	func(MMC, mmc, 0) \
	func(USB, usb, 0) \
	func(PXE, pxe, na) \
	func(DHCP, dhcp, na)
#include <config_distro_bootcmd.h>

#define CONFIG_EXTRA_ENV_SETTINGS \
	"dhcpuboot=usb start; dhcp u-boot.uimg; bootm\0" \
	ENV_DEVICE_SETTINGS \
	ENV_MEM_LAYOUT_SETTINGS \
	BOOTENV


#endif
